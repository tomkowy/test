import React, { Component } from 'react';
import logo from '../logo.svg';
import './app.css';
// import { GoogleLogin, GoogleLogout } from 'react-google-login';

class App extends Component {

  render() {

  const responseGoogle = (response) => {
      console.log(response);
  }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>

            {/* <GoogleLogin
                clientId="65241287201-9486hj4b135ge7h2li9l0otgbc0i5p66.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
            />
            <GoogleLogout
                buttonText="Logout"
                onLogoutSuccess={responseGoogle}
            /> */}
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
